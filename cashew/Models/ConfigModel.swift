//
//  ConfigModel.swift
//  livenow
//
//  Created by Hoang Nguyen on 5/28/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import Foundation

class ConfigModel {
    
    var user = UserDefaults.standard
    
    public func initForStarting() {
        
    }
    
    
    public func getString(key: String) -> String {
        var string:String = ""
        if let url = user.object(forKey: key) {
            string = url as! String
        }
        return string
    }

}
