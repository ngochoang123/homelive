//
//  LiveViewController.swift
//  livenow
//
//  Created by Hoang Nguyen on 5/25/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import UIKit
import LFLiveKit
import PopupDialog
import MZTimerLabel
import SwiftOverlays
import Photos

class LiveViewController: UIViewController, LFLiveSessionDelegate {
    
    @IBOutlet weak var liveView: UIView!
    @IBOutlet weak var startLive: UIButton!
    @IBOutlet weak var shareLive: UIButton!
    @IBOutlet weak var flash: UIButton!
    @IBOutlet weak var audio: UIButton!
    @IBOutlet weak var switchCamera: UIButton!
    @IBOutlet weak var liveStatus: UILabel!
    @IBOutlet weak var liveTime: MZTimerLabel!
    @IBOutlet weak var zoomOut: UIButton!
    @IBOutlet weak var zoomIn: UIButton!
    @IBOutlet weak var capture: UIButton!
    @IBOutlet weak var startOrStop: UIButton!
    
    var streamUrl : String? = nil
    var streamKey : String? = nil
    var streamShare : String? = nil
    var isReccoring: Bool = false
    
    let config = ConfigModel()
    let zoomLevel:CGFloat = 10.0
    var lastZoomFactor: CGFloat = 1.0
    var session: LFLiveSession = {
        
        let audioConfiguration = LFLiveAudioConfiguration.defaultConfiguration(for: LFLiveAudioQuality.veryHigh)
        let videoConfiguration = LFLiveVideoConfiguration.defaultConfiguration(for: LFLiveVideoQuality.high3)

        videoConfiguration?.sessionPreset = .captureSessionPreset720x1280
        videoConfiguration?.outputImageOrientation = .landscapeRight
        videoConfiguration?.autorotate = false
        videoConfiguration?.videoSize = CGSize(width: 1280, height: 720)
        
        let session = LFLiveSession(audioConfiguration: audioConfiguration, videoConfiguration: videoConfiguration)
        return session!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupForStart()
        self.style()
        //self.startingLive()
    }
    
    func liveSession(_ session: LFLiveSession?, debugInfo: LFLiveDebug?) {
        print("debugInfo: \(String(describing: debugInfo?.currentBandwidth))")
    }
    
    func liveSession(_ session: LFLiveSession?, errorCode: LFLiveSocketErrorCode) {
        alert(message: "Got an error \(errorCode.rawValue)")
        endLive()
    }
    
    func liveSession(_ session: LFLiveSession?, liveStateDidChange state: LFLiveState) {
        print("liveStateDidChange: \(state.rawValue)")
        switch state {
        case LFLiveState.ready:
            connecting()
            break;
            
        case LFLiveState.pending:
            connecting()
            break;
            
        case LFLiveState.start:
            started()
            break;
            
        case LFLiveState.error:
            endLive()
            alert(message: "Cannot start live stream.")
            break;
            
        case LFLiveState.stop:
            endLive()
            break;
            
        default:
            break;
        }
    }
    
    func connecting() {
        //liveTime.isHidden = true
        liveTime.reset()
        liveStatus.isHidden = true
    }
    
    func started() {
        isReccoring = true
        //liveTime.isHidden = false
        liveStatus.isHidden = false
        liveView.isHidden = false
        liveTime.start()

        self.removeAllOverlays()
    }
    
    func endLive() {
        isReccoring = false
        liveStatus.isHidden = true
        startLive.isSelected = false
        //liveTime.isHidden = true
        liveView.isHidden = true
        liveTime.reset()
        self.removeAllOverlays()
    }
    
    func startingLive() {
        
        let stream = LFLiveStreamInfo()
        stream.url = streamUrl
        if streamKey != "" {
            var streamWithKey = streamUrl! + "/" + streamKey!
            if streamUrl?.last == "/" {
                streamWithKey = streamUrl! + streamKey!
            }
            stream.url = streamWithKey
            print(streamWithKey)
        }
        session.startLive(stream)
        //Setup view
        //liveTime.isHidden = true
        startLive.isSelected = true
        liveTime.reset()
        liveStatus.isHidden = true
        
        let text = "Connecting ..."
        self.showWaitOverlayWithText(text)
    }
    
    @IBAction func capture(_ sender: UIButton) {
        
        self.setView(flag : true)
        self.capture.isHidden = true
        
        UIGraphicsBeginImageContextWithOptions(session.preView.bounds.size, session.preView.isOpaque, 0.0)
        self.session.preView.drawHierarchy(in: session.preView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        CustomPhotoAlbum().saveImage(image!)
        
        self.setView(flag : false)
        let when = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.capture.isHidden = false
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let value =  UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
        self.checkPhotoAuthorizationStatus()
    }
    
    func checkPhotoAuthorizationStatus(){
        PHPhotoLibrary.requestAuthorization { status in
            switch status {
            case .restricted, .denied:
                self.capture.isHidden = true
            default:
                self.capture.isHidden = false
                break
            }
        }
    }

    
    func setView(flag : Bool) {
        self.flash.isHidden = flag
        self.switchCamera.isHidden = flag
        self.shareLive.isHidden = flag
        self.liveView.isHidden = flag
        self.zoomIn.isHidden = flag
        self.zoomOut.isHidden = flag
        self.audio.isHidden = flag
        self.startOrStop.isHidden = flag
        
        if (!startLive.isSelected) {
            self.liveView.isHidden = true
        }
    }
}

extension LiveViewController {
    func alert(message : String) {
        let popup = PopupDialog(title: "Live Now", message: message, gestureDismissal: false)
        let buttonOk = DefaultButton(title: "OK") {
            print("Press Ok")
        }
        popup.addButton(buttonOk)
        present(popup, animated: true, completion: nil)
    }
}
//
//extension PopupDialog {
//    override public var prefersStatusBarHidden : Bool {
//        return false
//    }
//}

extension LiveViewController {
    
    @IBAction func share(_ sender: UIButton) {
        if self.streamShare != "" {
            let url = self.streamShare
            let activityVC = UIActivityViewController(activityItems:[url!], applicationActivities: nil)
            if DeviceType.IS_IPAD {
                let popup: UIPopoverController = UIPopoverController(contentViewController: activityVC)
                popup.present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }else {
                self.present(activityVC, animated: true, completion: nil)
            }
        } else {
            alert(message: "You are not config share live URL !")
        }
    }

    
    @IBAction func flash(_ sender: Any) {
        session.torch = !session.torch
        flash.isSelected = session.torch
    }
    
    @IBAction func switchCamera(_ sender: Any) {
        let devicePositon = session.captureDevicePosition;
        session.captureDevicePosition = (devicePositon == AVCaptureDevice.Position.back) ? AVCaptureDevice.Position.front : AVCaptureDevice.Position.back;
    }

    
    @IBAction func audio(_ sender: Any) {
        session.muted = !session.muted
        audio.isSelected = session.muted
    }
    
    @IBAction func zoomCamera(_ sender: UIPinchGestureRecognizer) {
        
        // Return zoom value between the minimum and maximum zoom values
        func minMaxZoom(_ factor: CGFloat) -> CGFloat {
            return min(min(max(factor, 1), zoomLevel), zoomLevel)
        }
        
        func update(scale factor: CGFloat) {
            session.zoomScale = factor
        }
        
        let newScaleFactor = minMaxZoom(sender.scale * lastZoomFactor)
        
        switch sender.state {
        case .began: fallthrough
        case .changed: update(scale: newScaleFactor)
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            update(scale: lastZoomFactor)
        default: break
        }
        
    }
    
    @IBAction func zoomIn(_ sender: UIButton) {
        lastZoomFactor -= 1
        if (lastZoomFactor < 1) {
            lastZoomFactor = 1
        }
        session.zoomScale = lastZoomFactor
    }
    
    @IBAction func zoomOut(_ sender: UIButton) {
        lastZoomFactor += 1
        if (lastZoomFactor > zoomLevel) {
            lastZoomFactor = zoomLevel
        }
        session.zoomScale = lastZoomFactor        
    }
    
    @IBAction func start(_ sender: UIButton) {
        
        if (startLive.isSelected) {
            session.stopLive()
            endLive()
        } else {
            self.startingLive()
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
}

extension LiveViewController {
    
    func style(){
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(LiveViewController.blinkLive), userInfo: nil, repeats: true)
    }
    
    @objc func blinkLive(){
        if(isReccoring){
            if(liveStatus.isHidden == true){
                liveStatus.isHidden = false
            }else{
                liveStatus.isHidden = true
            }
        }else {
            liveStatus.isHidden = true
        }
    }
    
    func setupForStart(){
        session.delegate = self
        session.preView = self.view
        session.captureDevicePosition = .back
        session.running = true
        session.mirror = false
        session.beautyFace = false
        session.beautyLevel = 0.0
        liveView.isHidden = true
    }
}

