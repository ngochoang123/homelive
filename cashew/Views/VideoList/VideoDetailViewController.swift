//
//  VideoDetailViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 2/15/18.
//  Copyright © 2018 Hoang Nguyen. All rights reserved.
//

import UIKit
import SwiftOverlays
import SwiftyJSON

class VideoDetailViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    
    var video:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self
        
        let videoLink = App.videoDetail + self.video["id"].string!
        let url = NSURL(string: videoLink)
        let request = NSURLRequest(url: url! as URL)
        
        self.webView.loadRequest(request as URLRequest)
        self.setTitle(title: self.video["title"].string!, subtitle: self.video["subtitle"].string!)
        
        SwiftOverlays.showBlockingWaitOverlayWithText("Loading ...")
    }
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.textColor = UIColor.white
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.textColor = UIColor.white
        two.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        self.navigationItem.titleView = stackView
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SwiftOverlays.removeAllBlockingOverlays()
    }
    
}
