//
//  VideoListViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 2/14/18.
//  Copyright © 2018 Hoang Nguyen. All rights reserved.
//

import UIKit
import SwiftOverlays
import Alamofire
import SwiftyJSON
import AlamofireImage

class VideoListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var registerPost: UIButton!
    
    let grayColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1)
    var videos = [JSON]()
    var liveData:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        
        self.loadVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkRequest()
    }
    
    @IBAction func registerPost(_ sender: UIButton) {
        // GO live
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var willRegister = false
        
        if let data = self.liveData {
            print(data)
            if data["status"].intValue == 3 {
                
                let viewController = storyboard.instantiateViewController(withIdentifier :"LiveViewController") as! LiveViewController
                viewController.streamUrl = data["data"]["stream_url"].stringValue
                viewController.streamKey = data["data"]["stream_key"].stringValue
                viewController.streamShare = data["data"]["share_url"].stringValue
                self.present(viewController, animated: true)
                
            } else if data["status"].intValue == 1 {
                willRegister = true
            } else {
                Alert.sharedInstance.showOk("HomeLive", message: "Tài khoản của bạn đang chờ duyệt.")
            }
            
        } else {
            willRegister = true
            
        }
        if (willRegister) {
            let viewController = storyboard.instantiateViewController(withIdentifier :"PostTableViewController") as! PostTableViewController
            let nav = UINavigationController(rootViewController: viewController)
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func checkRequest() {

        Alamofire.request(App.url,method: .get, parameters: [
            "uid" : UIDevice.current.identifierForVendor!.uuidString
            
            ]).responseJSON { response in
                if response.result.value != nil {
                    let data = JSON(response.result.value!)
                    let status = data["status"].intValue
                    self.liveData = data
                    if status == 3 {
                        self.registerPost.setTitle("LIVE", for: .normal)
                    } else {
                        self.registerPost.setTitle("ĐĂNG KÝ ĐĂNG TIN", for: .normal)
                    }
                }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    @IBAction func openAbout(_ sender: UIBarButtonItem) {
        UIApplication.shared.openURL(NSURL(string: "http://xemnhatructiep.com")! as URL)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoListTableViewCell", for: indexPath) as! VideoListTableViewCell
        let row = self.videos[indexPath.row]
        let image = NSURL(string: row["youtube_thumbnail"].string!)
        
        cell.backgroundColor = UIColor.clear
        cell.videoImage.af_setImage(withURL: image! as URL)
        cell.videoTitle.text = row["title"].string!
        cell.videoSubTitle.text = row["subtitle"].string!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"VideoDetailViewController") as! VideoDetailViewController
        viewController.video = self.videos[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func loadVideo() {
        SwiftOverlays.showBlockingWaitOverlayWithText("Please wait ...")
        
        Alamofire.request(App.videoList,method: .get, parameters: nil).responseJSON { response in
            
            if response.result.value != nil {
                let data = JSON(response.result.value!)
                for video in data.array! {
                    self.videos.append(video)
                }
            } else {
                Alert.sharedInstance.showOk("HomeLive", message: (response.result.error?.localizedDescription)!)
            }
            self.tableView.reloadData()
            SwiftOverlays.removeAllBlockingOverlays()
        }
    }

}
