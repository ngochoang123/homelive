//
//  VideoListTableViewCell.swift
//  cashew
//
//  Created by Hoang Nguyen on 2/14/18.
//  Copyright © 2018 Hoang Nguyen. All rights reserved.
//

import UIKit

class VideoListTableViewCell: UITableViewCell {

    @IBOutlet var videoImage: UIImageView!
    @IBOutlet var videoTitle: UILabel!
    @IBOutlet var videoSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
