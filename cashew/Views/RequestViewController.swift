//
//  RequestViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 9/16/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import UIKit
import SwiftOverlays
import Alamofire

class RequestViewController: UIViewController {

    @IBOutlet weak var liveTitle: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mainInput: UIView!
    
    var oldY:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RequestViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: UIResponder.keyboardDidShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: UIResponder.keyboardDidHideNotification , object: nil)
        
        self.oldY = self.view.frame.origin.y
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func openWebsite(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://cashewtrade.com.vn/en/live/")! as URL)
    }
    
    @IBAction func sendRequest(_ sender: UIButton) {
        
        let title = self.liveTitle.text?.trim()
        let email = self.email.text?.trim()
        
        if title == "" {
            Alert.sharedInstance.showOk("HomeLive", message: "Please put title or name of commodities.")
        } else if email == "" {
            Alert.sharedInstance.showOk("HomeLive", message: "Please put your email or phone number.")
        } else {
            
            SwiftOverlays.showBlockingWaitOverlayWithText("Please wait ...")
            
            Alamofire.request(App.url,method: .get, parameters: [
                "title" : title!,
                "email" : email!,
                "uid" : UIDevice.current.identifierForVendor!.uuidString
                
            ]).responseJSON { response in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"RequestSentViewController") as! RequestSentViewController
                viewController.modalTransitionStyle = .crossDissolve
                self.present(viewController, animated: true)
                
                SwiftOverlays.removeAllBlockingOverlays()
            }
        }
        
    }
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        self.view.frame.origin.y = self.oldY - 120
     
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        
        self.view.frame.origin.y = self.oldY
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let value =  UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }


}
