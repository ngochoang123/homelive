//
//  GoLiveViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 10/1/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftOverlays
import SwiftyJSON
import AVFoundation
import MessageUI

class GoLiveViewController: UIViewController, MFMailComposeViewControllerDelegate {

    var liveStreamInfo:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.requestAccessForVideo()
        self.requestAccessForAudio()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goLive(_ sender: UIButton) {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video)))
        if (status == AVAuthorizationStatus.authorized) {
            //let text = "Please wait..."
            //self.showWaitOverlayWithText(text)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"LiveViewController") as! LiveViewController
            viewController.streamUrl = self.liveStreamInfo["data"]["stream_url"].stringValue
            viewController.streamKey = self.liveStreamInfo["data"]["stream_key"].stringValue
            viewController.streamShare = self.liveStreamInfo["data"]["share_url"].stringValue
            self.present(viewController, animated: true)
            
        } else {
            Alert.sharedInstance.showOk("Live Now", message: "Permission to access camera was denied. You need to enable it on setting.")
            
        }
    }
    
    @IBAction func openWebsite(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://cashewtrade.com.vn/en/live/")! as URL)
        
    }
    
    @IBAction func openFacebook(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://fb.com/cashewtrade")! as URL)
    }
    
    @IBAction func openFacebookGroup(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://fb.com/groups/cashewtrade")! as URL)
    }
    
    @IBAction func sendMail(_ sender: UIButton) {
        
        let composer = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            composer.mailComposeDelegate = self
            composer.setToRecipients(["cashewtradevietnam@gmail.com"])
            composer.setSubject("[HomeLive] contact")
            present(composer, animated: true, completion: nil)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callMobile(_ sender: UIButton) {
        let url: NSURL = URL(string: "telprompt://+84938810048")! as NSURL
        UIApplication.shared.openURL(url as URL)
    }
    
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let value =  UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }

}



extension GoLiveViewController {
    func requestAccessForVideo() -> Void {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video)));
        switch status  {
        case AVAuthorizationStatus.notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video)), completionHandler: { (granted) in
            })
            break
            
        default:
            break
            
        }
    }
    
    func requestAccessForAudio() -> Void {
        let status = AVCaptureDevice.authorizationStatus(for:AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.audio)))
        switch status  {
        case AVAuthorizationStatus.notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.audio)), completionHandler: { (granted) in
                
            })
            break;
        default:
            break
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVMediaType(_ input: AVMediaType) -> String {
	return input.rawValue
}
