//
//  RequestSentViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 10/1/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftOverlays
import SwiftyJSON
import AVFoundation
import MessageUI

class RequestSentViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(RequestSentViewController.CheckRequestStatus(notification:)), name: Notification.Name("CheckRequestStatus"), object: nil)

    }
    
    @objc func CheckRequestStatus(notification: Notification) {
        self.checkRequest()
    }
 
    func checkRequest() {
        SwiftOverlays.showBlockingWaitOverlayWithText("Please wait ...")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        Alamofire.request(App.url,method: .get, parameters: [
            "uid" : UIDevice.current.identifierForVendor!.uuidString
            
            ]).responseJSON { response in
                if response.result.value != nil {
                    let data = JSON(response.result.value!)
                    let status = data["status"].intValue
                    
                    if status == 3 {
                        // GO live
                        let viewController = storyboard.instantiateViewController(withIdentifier :"GoLiveViewController") as! GoLiveViewController
                        viewController.liveStreamInfo = data
                        viewController.modalTransitionStyle = .crossDissolve
                        self.present(viewController, animated: true)
                    }
                }
                SwiftOverlays.removeAllBlockingOverlays()
                
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let value =  UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
        self.checkRequest()
    }
    
    @IBAction func openWebsite(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://www.cashewtrade.com.vn/en/live")! as URL)

    }
    
    @IBAction func openFacebook(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://fb.com/cashewtrade")! as URL)
    }
    
    @IBAction func openFacebookGroup(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "http://fb.com/groups/cashewtrade")! as URL)
    }
    
    @IBAction func sendMail(_ sender: UIButton) {
        
        let composer = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            composer.mailComposeDelegate = self
            composer.setToRecipients(["cashewtradevietnam@gmail.com"])
            composer.setSubject("[HomeLive] contact")
            present(composer, animated: true, completion: nil)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {

        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callMobile(_ sender: UIButton) {
        let url: NSURL = URL(string: "telprompt://+84938810048")! as NSURL
        UIApplication.shared.openURL(url as URL)

    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
}
