//
//  StatusBarShowingViewController.swift
//  ParkingBuddy
//
//  Created by Nguyen Hoang on 05/24/2017.
//  Copyright © 2015 Coded.dk. All rights reserved.
//

import UIKit

class StatusBarShowingViewController: UIViewController {
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
}
