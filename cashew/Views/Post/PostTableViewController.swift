//
//  PostTableViewController.swift
//  cashew
//
//  Created by Hoang Nguyen on 2/14/18.
//  Copyright © 2018 Hoang Nguyen. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import MMNumberKeyboard
import Alamofire
import SwiftOverlays

class PostTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var isSell: BetterSegmentedControl!
    @IBOutlet var type: BetterSegmentedControl!
    @IBOutlet var city: BetterSegmentedControl!
    @IBOutlet var street: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var area: UITextField!
    @IBOutlet var price: UITextField!
    @IBOutlet var direction: UITextField!
    @IBOutlet var front_of: UITextField!
    @IBOutlet var floor_count: UITextField!
    @IBOutlet var room_count: UITextField!
    @IBOutlet var toilet_count: UITextField!
    @IBOutlet var other: UITextField!
    @IBOutlet var priceType: UILabel!
    @IBOutlet var priceTypeLabel: UILabel!
    
    @IBOutlet var wardLabel: UILabel!
    @IBOutlet var districtLabel: UILabel!
    @IBOutlet var document1: UIImageView!
    @IBOutlet var document2: UIImageView!
    @IBOutlet var document3: UIImageView!
    @IBOutlet var document4: UIImageView!
    
    var district:String!
    var ward:String!
    var price_type:String!
    
    var documentAttach1:UIImage?
    var documentAttach2:UIImage?
    var documentAttach3:UIImage?
    var documentAttach4:UIImage?
    
    var imagePicker:UIImagePickerController!
    
    var currentPhoto:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    func setUpView() {
        //self.tableView.separatorStyle = .none
       // self.tableView.tableFooterView = UIView()
        
        let font = UIFont.systemFont(ofSize: 14)
        
        self.isSell.titles = ["Bán", "Cho thuê"]
        self.isSell.titleFont = font
        self.isSell.selectedTitleFont = font
        
        self.type.titles = ["Nhà", "Căn hộ"]
        self.type.titleFont = font
        self.type.selectedTitleFont = font
        
        self.city.titles = ["TP.HCM", "Hà Nội"]
        self.city.titleFont = font
        self.city.selectedTitleFont = font
        
        let phoneInput = MMNumberKeyboard()
        phoneInput.allowsDecimalPoint = true
        
        let areaInput = MMNumberKeyboard()
        areaInput.allowsDecimalPoint = true
        
        let priceInput = MMNumberKeyboard()
        priceInput.allowsDecimalPoint = true
        
        let frontOfInput = MMNumberKeyboard()
        frontOfInput.allowsDecimalPoint = true
        
        let floorInput = MMNumberKeyboard()
        let roomInput = MMNumberKeyboard()
        let toiletInput = MMNumberKeyboard()
        
        self.phone.inputView = phoneInput
        self.area.inputView = areaInput
        self.price.inputView = priceInput
        self.front_of.inputView = frontOfInput
        self.floor_count.inputView = floorInput
        self.room_count.inputView = roomInput
        self.toilet_count.inputView = toiletInput
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(takePhoto))
        document1.addGestureRecognizer(tapGestureRecognizer)
        document1.tag = 1
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(takePhoto))
        document2.addGestureRecognizer(tapGestureRecognizer2)
        document2.tag = 2
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(takePhoto))
        document3.addGestureRecognizer(tapGestureRecognizer3)
        document3.tag = 3
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(takePhoto))
        document4.addGestureRecognizer(tapGestureRecognizer4)
        document4.tag = 4
        
        let tapGestureRecognizerDistrict = UITapGestureRecognizer(target: self, action: #selector(selectDistrict))
        districtLabel.addGestureRecognizer(tapGestureRecognizerDistrict)
        
        let tapGestureRecognizerWard = UITapGestureRecognizer(target: self, action: #selector(selectWard))
        wardLabel.addGestureRecognizer(tapGestureRecognizerWard)
        
        let tapGestureRecognizerPriceType = UITapGestureRecognizer(target: self, action: #selector(selectPriceType))
        priceType.addGestureRecognizer(tapGestureRecognizerPriceType)
        
    }
    
    @objc func selectPriceType(_ sender: UITapGestureRecognizer) {
        var pickerData:[[String: String]]!
        
        if self.isSell.index == 0 {
            pickerData = [
                [
                    "name" : "Triệu"
                ],
                [
                    "name" : "Tỷ"
                ]
            ]
        } else {
            pickerData = [
                [
                    "name" : "Triệu/Tháng"
                ]
            ]
        }
        
        PickerDialog().show(title: "Chọn giá", options: pickerData, selected: self.priceTypeLabel.text) {
            (value) -> Void in
            self.priceTypeLabel.text = value
        }
    }
    
    @objc func selectDistrict(_ sender: UITapGestureRecognizer) {
        
        var pickerData = Address.Hcm
        
        if self.city.index == 1 {
            pickerData = Address.HaNoi
        }
        
        PickerDialog().show(title: "Chọn Quận/Huyện", options: pickerData, selected: self.districtLabel.text) {
            (value) -> Void in
            self.districtLabel.text = value
            self.setDefaultWard()
        }
    }

    
    @objc func selectWard(_ sender: UITapGestureRecognizer) {
        var pickerData:[[String: String]]!
        
        if self.city.index == 0 {
            pickerData = self.exploreWard(data: Address.Hcm)
        } else {
            pickerData = self.exploreWard(data: Address.HaNoi)
        }
        
        PickerDialog().show(title: "Chọn Phường/Xã", options: pickerData, selected: self.wardLabel.text) {
            (value) -> Void in
            self.wardLabel.text = value
        }
    }
    
    func setDefaultWard() {
        var pickerData:[[String: String]]!
        
        if self.city.index == 0 {
            pickerData = self.exploreWard(data: Address.Hcm)
        } else {
            pickerData = self.exploreWard(data: Address.HaNoi)
        }
        
        self.wardLabel.text = pickerData[0]["name"]
    }
    
    func exploreWard(data: [[String: String]]) -> [[String: String]] {
        
        let value = self.districtLabel.text
        
        for (_, dictionary) in data.enumerated() {
            if dictionary["name"] == value {
                return self.createPickerDataFromString(ward: dictionary["commune"]!)
            }
        }
        
        return [
            [
                "Unknow": "Unknow"
            ]
        ]
    }
    
    func createPickerDataFromString(ward: String) -> [[String: String]]  {
       
        let string = ward.components(separatedBy: "/")
        var pickerData = [[String: String]]()
        
        for (_, text) in string.enumerated() {
            pickerData.append([
                "name" : text
            ])
        }
        
        return pickerData
    }
    
    @objc func takePhoto(_ sender: UITapGestureRecognizer) {
        let image = sender.view as! UIImageView
        currentPhoto = image.tag
        present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)


        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        let newImage = self.resizeImage(image: image!, targetSize: CGSize(width: 1024, height: 1024))
        
        switch currentPhoto {
        case 1:
            self.document1.image = newImage
            self.documentAttach1 = newImage
        case 2:
            self.document2.image = newImage
            self.documentAttach2 = newImage
        case 3:
            self.document3.image = newImage
            self.documentAttach3 = newImage
        case 4:
            self.document4.image = newImage
            self.documentAttach4 = newImage
        default:
            break
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        let originImage = UIImage(named: "ic_camera")
        
        switch currentPhoto {
        case 1:
            self.document1.image = originImage
            self.documentAttach1 = nil
        case 2:
            self.document2.image = originImage
            self.documentAttach2 = nil
        case 3:
            self.document3.image = originImage
            self.documentAttach3 = nil
        case 4:
            self.document4.image = originImage
            self.documentAttach4 = nil
        default:
            break
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func isSell(_ sender: BetterSegmentedControl) {
        if sender.index == 0 {
            self.priceTypeLabel.text = "Giá bán"
            self.priceType.text = "Tỷ"
        } else {
            self.priceTypeLabel.text = "Giá cho thuê"
            self.priceType.text = "Triệu/tháng"
        }
    }
    
    @IBAction func type(_ sender: BetterSegmentedControl) {
        
    }
    
    @IBAction func city(_ sender: BetterSegmentedControl) {
        if sender.index == 0 {
            self.districtLabel.text = "Quận 1"
            self.wardLabel.text = "Phường Bến Nghé"
        } else {
            self.districtLabel.text = "Quận Ba Đình"
            self.wardLabel.text = "Phường Buởi"
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func closeView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func post(_ sender: UIButton) {
        
        if self.street.text == "" || self.phone.text == "" || self.area.text == "" || self.price.text == "" {
            Alert.sharedInstance.showOk("HomeLive", message: "Vui lòng nhập các thông tin bắt buộc.")
        } else {
            
            SwiftOverlays.showBlockingWaitOverlayWithText("Please wait ...")
            
            let parameters = [
                "uid" : UIDevice.current.identifierForVendor!.uuidString,
                "is_sell" : (self.isSell.index == 0) ? "1" : "0",
                "type" : String(self.type.index + 1),
                "city" : String(self.city.index + 1),
                "street" : self.street.text!,
                "district" : self.districtLabel.text!,
                "ward" : self.wardLabel.text!,
                "phone" : self.phone.text!,
                "area" : self.area.text!,
                "price": self.price.text!,
                "price_type": self.priceType.text!,
                "direction": self.direction.text!,
                "front_of" : self.front_of.text!,
                "floor_count" : self.floor_count.text!,
                "room_count" : self.room_count.text!,
                "toilet_count" : self.toilet_count.text!,
                "other" : self.other.text!
                ] as [String : Any]
       
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    
                    if let documentA = self.documentAttach1 {
                        if let imageData1 = documentA.pngData() {
                            multipartFormData.append(imageData1, withName: "document[0]", fileName: "file1.png", mimeType: "image/png")
                        }
                    }
                    
                    if let documentB = self.documentAttach2 {
                        if let imageData1 = documentB.pngData() {
                            multipartFormData.append(imageData1, withName: "document[1]", fileName: "file2.png", mimeType: "image/png")
                        }
                    }
                    
                    if let documentC = self.documentAttach3 {
                        if let imageData1 = documentC.pngData() {
                            multipartFormData.append(imageData1, withName: "document[2]", fileName: "file3.png", mimeType: "image/png")
                        }
                    }
                    
                    if let documentD = self.documentAttach4 {
                        if let imageData1 = documentD.pngData() {
                            multipartFormData.append(imageData1, withName: "document[3]", fileName: "file4.png", mimeType: "image/png")
                        }
                    }
                    
                    for (key, value) in parameters {
                        
                        let stringValue = value as! String
                        multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                    }
            },
                to: App.post,
                encodingCompletion: { encodingResult in
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseString { response in
                                self.dismiss(animated: true, completion: nil)
                                SwiftOverlays.removeAllBlockingOverlays()
                            }
                            .uploadProgress { progress in // main queue by default
                                print("Upload Progress: \(progress.fractionCompleted)")
                        }
                        return
                    case .failure(let encodingError):
                        SwiftOverlays.removeAllBlockingOverlays()
                        Alert.sharedInstance.showOk("Error", message: encodingError.localizedDescription)
                        debugPrint(encodingError)
                    }
                    
            })
            
            
        }
        
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
