import UIKit
import PopupDialog

class Alert: NSObject {
    
    var popupWindow : UIWindow!
    var rootVC : UIViewController!

    class var sharedInstance: Alert {
        struct SingletonWrapper {
            static let sharedInstance = Alert()
        }
        
        return SingletonWrapper.sharedInstance;
    }
    
    fileprivate override init() {
        let screenBounds = UIScreen.main.bounds
        popupWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: screenBounds.width, height: screenBounds.height))
        popupWindow.windowLevel = UIWindow.Level.statusBar + 1
        
        rootVC = StatusBarShowingViewController()
        popupWindow.rootViewController = rootVC
        
        super.init()
    }

    func showOk(_ title: String, message: String, onComplete: @escaping ()->Void = {  }) {
        popupWindow.isHidden = false
        let popup = PopupDialog(title: title, message: message, gestureDismissal: false)
        let buttonOk = DefaultButton(title: "OK") {
            self.resignPopupWindow()
            print("Press Ok")
        }
        popup.addButton(buttonOk)
        rootVC.present(popup, animated: true, completion: nil)
    }
    
    func resignPopupWindow() {
        self.popupWindow.isHidden = true
    }
    
}
