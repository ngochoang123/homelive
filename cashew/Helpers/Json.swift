//
//  Json.swift
//  livenow
//
//  Created by Hoang Nguyen on 5/25/17.
//  Copyright © 2017 Hoang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

struct JSONBodyStringEncoding: ParameterEncoding {
    private let jsonBody: String
    
    init(jsonBody: String) {
        self.jsonBody = jsonBody
    }
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = urlRequest.urlRequest
        let dataBody = (jsonBody as NSString).data(using: String.Encoding.utf8.rawValue)
        if urlRequest?.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest?.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        urlRequest?.httpBody = dataBody
        return urlRequest!
    }
}
