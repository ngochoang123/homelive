//
//  Helpers.swift
//  YouTubeLiveVideo
//
//  Created by Nguyen Hoang on 05/24/2017.
//  Copyright © 2017 Nguyen Hoang. All rights reserved.
//

import Foundation
import Photos
import UIKit

class Helpers {
    
    class func dateAfter(_ date: Date, after: (hour: NSInteger, minute: NSInteger, second: NSInteger)) -> Date {
        let calendar = Calendar.current
        if let date = (calendar as NSCalendar).date(byAdding: .hour, value: after.hour, to: date, options: []) {
            if let date = (calendar as NSCalendar).date(byAdding: .minute, value: after.minute, to: date, options: []) {
                if let date = (calendar as NSCalendar).date(byAdding: .second, value: after.second, to: date, options: []) {
                    return date
                }
            }
        }
        return date
    }
    
}


class CustomPhotoAlbum {
    
    static let albumName = "HomeLive"
    static let sharedInstance = CustomPhotoAlbum()
    
    var assetCollection: PHAssetCollection!
    
    init() {
        
        func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
            let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
            
            if let first_Obj:PHAssetCollection = collection.firstObject{
                return first_Obj
            }
            
            return nil
        }
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)
        }) { success, _ in
            if success {
                self.assetCollection = fetchAssetCollectionForAlbum()
            }
        }
    }
    
    func saveImage(_ image: UIImage) {
        
        if assetCollection == nil {
            return   // If there was an error upstream, skip the save.
        }
        
        PHPhotoLibrary.shared().performChanges({
            
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            let enumeration: NSArray = [assetPlaceholder!]
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            albumChangeRequest!.addAssets(enumeration)
        }, completionHandler: nil)
    }
    
    
}



