//
//  Constants.swift
//  YouTubeLiveVideo
//
//  Created by Nguyen Hoang on 05/24/17.
//  Copyright © 2017 Nguyen Hoang. All rights reserved.
//

import UIKit

struct App {
    static let mainColor = UIColor.init(red: 230/255, green: 33/255, blue: 23/255, alpha: 1)
    static let url = "http://cashewtrade.com.vn/app/homelive/"
    static let videoList = url + "list.php"
    static let videoDetail = url + "detail.php?id="
    static let post = url + "index.php"
}

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_5_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
    static let IS_IPHONE_6_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 667.0

    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
}

let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)

struct Address
{
    static let HaNoi  = [
        [
            "name" : "Quận Ba Đình",
            "commune" : "Phường Bưởi/Phường Cầu Giấy/Phường Cống Vị/Phường Điện Biên/Phường Đội Cấn/Phường Giảng Võ/Phường Kim Mã/Phường Ngọc Hà/Phường Nguyễn Trung Trực/Phường Phúc Xá/Phường Quán Thánh/Phường Thành Công/Phường Thụy Khuê/Phường Trúc Bạnh/Phường Yên Phụ"
        ],
        [
            "name" : "Quận Cầu Giấy",
            "commune" : "Phường Dịch Vọng/Phường Dịch Vọng Hậu/Phường Mai Dịch/Phường Nghĩa Đô/Phường Nghĩa Tân/Phường Quan Hoa/Phường Trung Hòa/Phường Yên Hòa"
        ],
        [
            "name" : "Quận Đống Đa",
            "commune" : "Phường Bích Câu/Phường Cát Linh/Phường Hàng Bột/Phường Khâm Thiên/Phường Khương Thượng/Phường Kim Liên/Phường Láng Hạ/Phường Láng Thượng/Phường Nam Đồng/Phường Ngã Tư Sở/Phường Ô Chợ Dừa/Phường Phương Liên/Phường Phương Mai/Phường Quang Trung/Phường Quốc Tử Giám/Phường Thịnh Quang/Phường Thổ Quan/Phường Trung Liệt/Phường Trung Phụng/Phường Trung Tự/Phường Văn Chương/Phường Văn Miếu"

        ],
        [
            "name" : "Quận Hà Đông",
            "commune" : "Phường Biên Giang/Phường Dương Nội/Phường Hà Cầu/Phường Kiến Hưng/Phường La Khê/Phường Mộ Lao/Phường Nguyễn Trãi/Phường Phú La/Phường Phú Lãm/Phường Phú Lương/Phường Phúc La/Phường Quan Trung/Phường Vạn Phúc/Phường Văn Quán/Phường Yên Nghĩa/Phường Yết Kiêu"

        ],
        [
            "name" : "Quận Hai Bà Trưng",
            "commune" : "Phường Bạch Đằng/Phường Bách Khoa/Phường Bạch Mai/Phường Bùi Thị Xuân/Phường Cầu Dền/Phường Đống Mác/Phường Đồng Nhân/Phường Đồng Tâm/Phường Lê Đại Hành/Phường Minh Khai/Phường Ngô Thì Nhậm/Phường Nguyễn Du/Phường Phạm Đình Hổ/Phường Phố Huế/Phường Quỳnh Lôi/Phường Quỳnh Mai/Phường Thanh Lương/Phường Thanh Nhàn/Phường Trương Định/Phường Vĩnh Tuy"

        ],
        [
            "name" : "Quận Hoàn Kiếm",
            "commune" : "Phường Chương Dương Độ/Phường Cửa Đông/Phường Cửa Nam/Phường Đồng Xuân/Phường Hàng Bông/Phường Hàng Gai/Phường Hàng Bạc/Phường Hàng Bài/Phường Hàng Bồ/Phường Hàng Buồm/Phường Hàng Đào/Phường Hàng Mã/Phường Hàng Trống/Phường Lý Thái Tổ/Phường Phan Chu Trinh/Phường Phúc Tân/Phường Tràng Tiền/Phường Trần Hưng Đạo"

        ],
        [
            "name" : "Quận Hoàng Mai",
            "commune" : "Phường Đại Kim/Phường Định Công/Phường Giáp Bát/Phường Hoàng Liệt/Phường Hoàng Văn Thụ/Phường Lĩnh Nam/Phường Mai Động/Phường Tân Mai/Phường Thanh Trì/Phường Thịnh Liệt/Phường Trần Phú/Phường Tương Mai/Phường Vĩnh Hưng/Phường Yên Sở"

        ],
        [
            "name" : "Quận Long Biên",
            "commune" : "Phường Bồ Đề/Phường Cự Khối/Phường Đức Giang/Phường Gia Thụy/Phường Long Biên/Phường Ngọc Lâm/Phường Ngọc Thụy/Phường Phúc Đồng/Phường Phúc Lợi/Phường Sài Đồng/Phường Thạch Bàn/Phường Thượng Thanh/Phường Việt Hưng"

        ],
        [
            "name" : "Quận Tây Hồ",
            "commune" : "Phường Bưởi/Phường Nhật Tân/Phường Phú Thượng/Phường Quảng An/Phường Thụy Khuê/Phường Tứ Liên/Phường Xuân La/Phường Yên Phụ"

        ],
        [
            "name" : "Quận Thanh Xuân",
            "commune" : "Phường Hạ Đình/Phường Khương Đình/Phường Khương Mai/Phường Khương Trung/Phường Kim Giang/Phường Nhân Chính/Phường Phương Liệt/Phường Thanh Xuân Bắc/Phường Thanh Xuân Nam/Phường Thanh Xuân Trung/Phường Thượng Đình"

        ],
        [
            "name" : "Huyện Ba Vì",
            "commune" : "Xã Ba Trại/Xã Ba Vì/Xã Cam Thượng/Xã Cẩm Lĩnh/Xã Châu Sơn/Xã Chu Minh/Xã Cổ Đô/Xã Đông Quang/Xã Đồng Thái/Xã Khánh Thượng/Xã Minh Châu/Xã Minh Quang/Xã Phong Vân/Xã Phú Châu/Xã Phú Cường/Xã Phú Đông/Xã Phú Phương/Xã Phú Sơn/Xã Sơn Đà/Xã Tản Hồng/Xã Tản Lĩnh/Xã Tây Đằng/Xã Thái Hòa/Xã Thuần Mỹ/Xã Thụy An/Xã Tiên Phong/Xã Tòng Bạt/Xã Vạn Thắng/Xã Vân Hòa/Xã Vật Lại/Xã Yên Bài"

        ],
        [
            "name" : "Huyện Chương Mỹ",
            "commune" : "Xã Chúc Sơn/Xã Đại Yên/Xã Đồng Lạc/Xã Đồng Phú/Xã Đông Phương Yên/Xã Đông Sơn/Xã Hòa Chính/Xã Hoàng Diệu/Xã Hoàng Văn Thụ/Xã Hồng Phong/Xã Hợp Đồng/Xã Hữu Văn/Xã Lam Điền/Xã Mỹ Lương/Xã Nam Phương Tiến/Xã Ngọc Hòa/Xã Phú Nam An/Xã Phú Nghĩa/Xã Phụng Châu/Xã Quảng Bị/Xã Tân Tiến/Xã Thanh Bình/Xã Thụy Hương/Xã Thủy Xuân Tiên/Xã Thượng Vực/Xã Tiên Phương/Xã Tốt Động/Xã Trần Phú/Xã Trung Hòa/Xã Trường Yên/Xã Văn Võ/Xã Xuân Mai"

        ],
        [
            "name" : "Huyện Đan Phượng",
            "commune" : "Thị trấn Phùng/Xã Đan Phượng/Xã Đồng Tháp/Xã Hồng Hà/Xã Liên Hà/Xã Liên Hồng/Xã Liên Trung/Xã Phương Đình/Xã Song Phượng/Xã Tân Hội/Xã Tân Lập/Xã Thọ An/Xã Thọ Xuân/Xã Thượng Mỗ/Xã Trung Châu"

        ],
        [
            "name" : "Huyện Đông Anh",
            "commune" : "Thị trấn Đông Anh/Xã Bắc Hồng/Xã Cổ Loa/Xã Dục Tú/Xã Đại Mạch/Xã Đông Hội/Xã Hải Bối/Xã Kim Chung/Xã Kim Nỗ/Xã Liên Hà/Xã Mai Lâm/Xã Nam Hồng/Xã Nguyên Khê/Xã Tầm Xá/Xã Thụy Lâm/Xã Tiên Dương/Xã Uy Nỗ/Xã Vân Hà/Xã Vân Nội/Xã Việt Hùng/Xã Vĩnh Ngọc/Xã Võng La/Xã Xuân Canh/Xã Xuân Nộn"

        ],
        [
            "name" : "Huyện Gia Lâm",
            "commune" : "Thị trấn Yên Viên/Xã Bát Tràng/Xã Cổ Bi/Xã Dương Hà/Xã Dương Quang/Xã Dương Xá/Xã Đa Tốn/Xã Đặng Xá/Xã Đình Xuyên/Xã Đông Dư/Xã Kiêu Kỵ/Xã Kim Lan/Xã Kim Sơn/Xã Lệ Chi/Xã Ninh Hiệp/Xã Phù Đổng/Xã Phú Thị/Xã Trâu Quỳ/Xã Trung Mầu/Xã Văn Đức/Xã Yên Thường/Xã Yên Viên"

        ],
        [
            "name" : "Huyện Hoài Đức",
            "commune" : "Xã An Khánh/Xã An Thượng/Xã Cát Quế/Xã Di Trạch/Xã Dương Liễu/Xã Đắc Sở/Xã Đông La/Xã Đức Giang/Xã Đức Thượng/Xã Kim Chung/Xã La Phù/Xã Lại Yên/Xã Minh Khai/Xã Song Phương/Xã Sơn Đồng/Xã Tiền Yên/Xã Trạm Trôi/Xã Vân Canh/Xã Vân Côn/Xã Yên Sở"

        ],
        [
            "name" : "Huyện Mê Linh",
            "commune" : "Xã Chi Đông/Xã Chu Phan/Xã Đại Thịnh/Xã Hoàng Kim/Xã Kim Hoa/Xã Liên Mạc/Xã Mê Linh/Xã Quang Minh/Xã Tam Đồng/Xã Thạch Đà/Xã Thanh Lâm/Xã Tiền Phong/Xã Tiến Thắng/Xã Tiến Thịnh/Xã Tráng Việt/Xã Tự Lập/Xã Vạn Yên/Xã Văn Khê"

        ],
        [
            "name" : "Huyện Mỹ Đức",
            "commune" : "Xã An Mỹ/Xã An Phú/Xã An Tiến/Xã Bột Xuyên/Xã Đại Hưng/Xã Đại Nghĩa/Xã Đốc Tín/Xã Đồng Tâm/Xã Hồng Sơn/Xã Hợp Thanh/Xã Hợp Tiến/Xã Hùng Tiến/Xã Hương Sơn/Xã Lê Thanh/Xã Mỹ Thành/Xã Phù Lưu Tế/Xã Phúc Lâm/Xã Phùng Xá/Xã Thượng Lâm/Xã Tuy Lai/Xã Vạn Kim/Xã Xuy Xá"

        ],
        [
            "name" : "Huyện Phú Xuyên",
            "commune" : "Thị trấn Phú Xuyên/Xã Châu Can/Xã Chuyên Mỹ/Xã Đại Thắng/Xã Đại Xuyên/Xã Hoàng Long/Xã Hồng Minh/Xã Hồng Thái/Xã Khai Thái/Xã Minh Tân/Xã Nam Phong/Xã Nam Triều/Xã Phú Minh/Xã Phú Túc/Xã Phú Yên/Xã Phúc Tiến/Xã Phượng Dực/Xã Quang Lãng/Xã Quang Trung/Xã Sơn Hà/Xã Tân Dân/Xã Thụy Phú/Xã Tri Thủy/Xã Tri Trung/Xã Văn Hoàng/Xã Văn Nhân/Xã Vân Từ"

        ],
        [
            "name" : "Huyện Phúc Thọ",
            "commune" : "Thị trấn Phúc Thọ/Xã Cẩm Đình/Xã Hát Môn/Xã Hiệp Thuận/Xã Liên Hiệp/Xã Long Xuyên/Xã Ngọc Tảo/Xã Phúc Hòa/Xã Phụng Thượng/Xã Phương Độ/Xã Sen Chiểu/Xã Tam Hiệp/Xã Tam Thuấn/Xã Thanh Đa/Xã Thọ Lộc/Xã Thượng Cốc/Xã Tích Giang/Xã Trạch Mỹ Lộc/Xã Vân Hà/Xã Vân Nam/Xã Vân Phúc/Xã Võng Xuyên/Xã Xuân Phú"

        ],
        [
            "name" : "Huyện Quốc Oai",
            "commune" : "Thị trấn Quốc Oai/Xã Cấn Hữu/Xã Cộng Hòa/Xã Đại Thành/Xã Đồng Quang/Xã Đông Xuân/Xã Đông Yên/Xã Hòa Trạch/Xã Liệp Tuyết/Xã Nghĩa Hương/Xã Ngọc Liệp/Xã Ngọc Mỹ/Xã Phú Cát/Xã Phú Mãn/Xã Phượng Cách/Xã Sài Sơn/Xã Tân Hòa/Xã Tân Phú/Xã Thạch Thán/Xã Tuyết Nghĩa/Xã Yên Sơn"

        ],
        [
            "name" : "Huyện Sóc Sơn",
            "commune" : "Thị trấn Sóc Sơn/Xã Bắc Phú/Xã Bắc Sơn/Xã Đông Xuân/Xã Đức Hòa/Xã Hiền Ninh/Xã Hồng Kỳ/Xã Kim Lũ/Xã Mai Đình/Xã Minh Phú/Xã Minh Trí/Xã Nam Sơn/Xã Phú Cường/Xã Phù Linh/Xã Phù Lỗ/Xã Phú Minh/Xã Quang Tiến/Xã Tân Dân/Xã Tân Hưng/Xã Tân Minh/Xã Thanh Xuân/Xã Tiên Dược/Xã Trung Giã/Xã Việt Long/Xã Xuân Giang/Xã Xuân Thu"

        ],
        [
            "name" : "Huyện Thạch Thất",
            "commune" : "Xã Bình Phú/Xã Bình Yên/Xã Canh Nậu/Xã Cẩm Yên/Xã Cần Kiệm/Xã Chàng Sơn/Xã Dị Nậu/Xã Đại Đồng/Xã Đồng Trúc/Xã Hạ Bằng/Xã Hương Ngải/Xã Hữu Bằng/Xã Kim Quan/Xã Lại Thượng/Xã Liên Quan/Xã Phú Kim/Xã Phùng Xá/Xã Tân Xã/Xã Thạch Hòa/Xã Thạch Xá/Xã Tiến Xuân/Xã Yên Bình/Xã Yên Trung"

        ],
        [
            "name" : "Huyện Thanh Oai",
            "commune" : "Xã Bích Hòa/Xã Bình Minh/Xã Cao Dương/Xã Cao Viên/Xã Cự Khê/Xã Dân Hòa/Xã Đỗ Động/Xã Hồng Dương/Xã Kim An/Xã Kim Bài/Xã Kim Thư/Xã Liên Châu/Xã Mỹ Hưng/Xã Phương Trung/Xã Tam Hưng/Xã Tân Ước/Xã Thach Cao/Xã Thanh Mai/Xã Thanh Thùy/Xã Thanh Văn/Xã Xuân Dương"

        ],
        [
            "name" : "Huyện Thanh Trì",
            "commune" : "Xã Duyên Hà/Xã Đại Áng/Xã Đông Mỹ/Xã Hữu Hòa/Xã Liên Ninh/Xã Ngọc Hồi/Xã Ngũ Hiệp/Xã Tả Thanh Oai/Xã Tam Hiệp/Xã Tân Triều/Xã Thanh Liệt/Xã Tứ Hiệp/Xã Vạn Phúc/Xã Văn Điển/Xã Vĩnh Quỳnh/Xã Yên Mỹ"

        ],
        [
            "name" : "Huyện Thường Tín",
            "commune" : "Thị trấn Thường Tín/Xã Chương Dương/Xã Dũng Tiến/Xã Duyên Thái/Xã Hà Hồi/Xã Hiền Giang/Xã Hòa Bình/Xã Hồng Vân/Xã Khánh Hà/Xã Lê Lợi/Xã Liên Phương/Xã Minh Cường/Xã Nghiêm Xuyên/Xã Nguyễn Trãi/Xã Nhị Khê/Xã Ninh Sở/Xã Quất Động/Xã Tân Minh/Xã Thắng Lợi/Xã Thống Nhất/Xã Thư Phú/Xã Tiền Phong/Xã Tô Hiệu/Xã Tự Nhiên/Xã Vạn Điểm/Xã Văn Bình/Xã Văn Phú/Xã Văn Tự/Xã Vân Tảo"

        ],
        [
            "name" : "Huyện Từ Liêm",
            "commune" : "Thị trấn Cầu Diễn/Xã Cổ Nhuế/Xã Đại Mỗ/Xã Đông Ngạc/Xã Liên Mạc/Xã Mễ Trì/Xã Minh Khai/Xã Mỹ Đình/Xã Phú Diễn/Xã Tây Mỗ/Xã Tây Tựu/Xã Thượng Cát/Xã Thụy Phương/Xã Trung Văn/Xã Xuân Đỉnh/Xã Xuân Phương"

        ],
        [
            "name" : "Huyện Ứng Hòa",
            "commune" : "Xã Cao Thành/Xã Đại Cường/Xã Đại Hùng/Xã Đội Bình/Xã Đông Lỗ/Xã Đồng Tân/Xã Đồng Tiến/Xã Hòa Lâm/Xã Hòa Nam/Xã Hòa Phú/Xã Hoa Sơn/Xã Hòa Xá/Xã Hồng Quang/Xã Kim Đường/Xã Liên Bạt/Xã Lưu Hoàng/Xã Minh Đức/Xã Phù Lưu/Xã Phương Tú/Xã Quảng Phú Cầu/Xã Sơn Công/Xã Tảo Dương Văn/Xã Trầm Lộng/Xã Trung Tú/Xã Trường Thịnh/Xã Vạn Thái/Xã Vân Đình/Xã Viên An/Xã Viên Nội"

        ],
        [
            "name" : "Thị xã Sơn Tây",
            "commune" : "Xã Cổ Đông/Xã Đường Lâm/Xã Kim Sơn/Xã Lê Lợi/Xã Ngô Quyền/Xã Phú Thịnh/Xã Quang Trung/Xã Sơn Đông/Xã Sơn Lộc/Xã Thanh Mỹ/Xã Trung Hưng/Xã Trung Sơn Trầm/Xã Viên Sơn/Xã Xuân Khanh/Xã Xuân Sơn"
        ]
    ]

    static let Hcm  = [
        [
            "name" : "Quận 1",
            "commune" : "Phường Bến Nghé/Phường Bến Thành/Phường Cầu Kho/Phường Cầu Ông Lãnh/Phường Cô Giang/Phường Đa Kao/Phường Nguyễn Cư Trinh/Phường Nguyễn Thái Bình/Phường Phạm Ngũ Lão/Phường Tân Định"
        ],
        [
            "name" : "Quận 2",
            "commune" : "Phường An Khánh/Phường An Lợi Đông/Phường An Phú/Phường Bình An/Phường Bình Khánh/Phường Bình Trưng Đông/Phường Bình Trưng Tây/Phường Cát Lái/Phường Thạnh Mỹ Lợi/Phường Thảo Điền/Phường Thủ Thiêm"
        ],
        [
            "name" : "Quận 3",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14"
        ],
        [
            "name" : "Quận 4",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 12/Phường 13/Phường 14/Phường 15/Phường 16/Phường 18"
        ],
        [
            "name" : "Quận 5",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15"
        ],
        [
            "name" : "Quận 6",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15"
        ],
        [
            "name" : "Quận 7",
            "commune" : "Phường Bình Thuận/Phường Phú Mỹ/Phường Phú Thuận/Phường Tân Hưng/Phường Tân Kiểng/Phường Tân Phong/Phường Tân Phú/Phường Tân Quy/Phường Tân Thuận Đông/Phường Tân Thuận Tây"
        ],
        [
            "name" : "Quận 8",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15/Phường 16"
        ],
        [
            "name" : "Quận 9",
            "commune" : "Phường Hiệp Phú/Phường Long Bình/Phường Long Phước/Phường Long Thạnh Mỹ/Phường Long Trường/Phường Phú Hữu/Phường Phước Bình/Phường Phước Long A/Phường Phước Long B/Phường Tân Phú/Phường Tăng Nhơn Phú A/Phường Tăng Nhơn Phú B/Phường Trường Thạnh"
        ],
        [
            "name" : "Quận 10",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15"
        ],
        [
            "name" : "Quận 11",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15/Phường 16"
        ],
        [
            "name" : "Quận 12",
            "commune" : "Phường An Phú Đông/Phường Đông Hưng Thuận/Phường Hiệp Thành/Phường Tân Chánh Hiệp/Phường Tân Hưng Thuận/Phường Tân Thới Hiệp/Phường Tân Thới Nhất/Phường Thạnh Lộc/Phường Thạnh Xuân/Phường Thới An/Phường Trung Mỹ Tây"
        ],
        [
            "name" : "Quận Bình Tân",
            "commune" : "Phường An Lạc/Phường An Lạc A/Phường Bình Hưng Hòa/Phường Bình Hưng Hòa A/Phường Bình Hưng Hòa B/Phường Bình Trị Đông A/Phường Bình Trị Đông B/Phường Tân Tạo/Phường Tân Tạo A"
        ],
        [
            "name" : "Quận Bình Thạnh",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15/Phường 17/Phường 19/Phường 21/Phường 22/Phường 24/Phường 25/Phường 26/Phường 27/Phường 28"
        ],
        [
            "name" : "Quận Gò Vấp",
            "commune" : "Phường 01/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15/Phường 16/Phường 17"
        ],
        [
            "name" : "Quận Phú Nhuận",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15/Phường 17"
        ],
        [
            "name" : "Quận Tân Bình",
            "commune" : "Phường 01/Phường 02/Phường 03/Phường 04/Phường 05/Phường 06/Phường 07/Phường 08/Phường 09/Phường 10/Phường 11/Phường 12/Phường 13/Phường 14/Phường 15"
        ],
        [
            "name" : "Quận Tân Phú",
            "commune" : "Phường Hiệp Tân/Phường Hòa Thạnh/Phường Phú Thạnh/Phường Phú Thọ Hòa/Phường Phú Trung/Phường Sơn Kỳ/Phường Tân Quý/Phường Tân Sơn Nhì/Phường Tân Thành/Phường Tân Thới Hòa/Phường Tây Thạnh"
        ],
        [
            "name" : "Quận Thủ Đức",
            "commune" : "Phường Bình Chiểu/Phường Bình Thọ/Phường Hiệp Bình Chánh/Phường Hiệp Bình Phước/Phường Linh Chiểu/Phường Linh Đông/Phường Linh Tây/Phường Linh Trung/Phường Linh Xuân/Phường Tam Bình/Phường Tam Phú/Phường Trường Thọ"
        ],
        [
            "name" : "Huyện Bình Chánh",
            "commune" : "Thị trấn Tân Túc/Xã An Phú Tây/Xã Bình Chánh/Xã Bình Hưng/Xã Bình Lợi/Xã Đa Phước/Xã Hưng Long/Xã Lê Minh Xuân/Xã Phạm Văn Hai/Xã Phong Phú/Xã Quy Đức/Xã Tân Kiên/Xã Tân Nhựt/Xã Tân Quý Tây/Xã Vĩnh Lộc A/Xã Vĩnh Lộc B"
        ],
        [
            "name" : "Huyện Cần Giờ",
            "commune" : "Thị trấn Cần Thạnh/Xã An Thới Đông/Xã Bình Khánh/Xã Long Hòa/Xã Lý Nhơn/Xã Tam Thôn Hiệp/Xã Thạnh An"
        ],
        [
            "name" : "Huyện Củ Chi",
            "commune" : "Thị trấn Củ Chi/Xã An Nhơn Tây/Xã An Phú/Xã Bình Mỹ/Xã Hòa Phú/Xã Nhuận Đức/Xã Phạm Văn Cội/Xã Phú Hòa Đông/Xã Phú Mỹ Hưng/Xã Phước Hiệp/Xã Phước Thạnh/Xã Phước Vĩnh An/Xã Tân An Hội/Xã Tân Phú Trung/Xã Tân Thạnh Đông/Xã Tân Thạnh Tây/Xã Tân Thông Hội/Xã Thái Mỹ/Xã Trung An/Xã Trung Lập Hạ/Xã Trung Lập Thượng"
        ],
        [
            "name" : "Huyện Hóc Môn",
            "commune" : "Thị trấn Hóc Môn/Xã Bà Điểm/Xã Đông Thạnh/Xã Nhị Bình/Xã Tân Hiệp/Xã Tân Thới Nhì/Xã Tân Xuân/Xã Thới Tam Thôn/Xã Trung Chánh/Xã Xuân Thới Đông/Xã Xuân Thới Sơn/Xã Xuân Thới Thượng"
        ],
        [
            "name" : "Huyện Nhà Bè",
            "commune" : "Thị trấn Nhà Bè/Xã Hiệp Phước/Xã Long Thới/Xã Nhơn Đức/Xã Phú Xuân/Xã Phước Kiển/Xã Phước Lộc"
        ]
    ]
}
